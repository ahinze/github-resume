<?php

namespace CoreBundle\Provider;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class GithubProvider
 */
class GithubProvider
{
    /** @var Client */
    private $guzzleClient;

    /** @var Client */
    private $guzzleAuthClient;

    /** @var string */
    private $clientId;

    /** @var string */
    private $clientSecret;

    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /**
     * GithubProvider constructor.
     * @param string $clientId
     * @param string $clientSecret
     * @param Session $session
     */
    public function __construct($clientId, $clientSecret, $session)
    {
        $this->guzzleClient = new Client(array('base_uri' => 'https://api.github.com/'));
        $this->guzzleAuthClient = new Client(array('base_uri' => 'https://github.com/'));
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->username = $session->get('github_username');
        $this->password = $session->get('github_password');
    }

    /**
     * @param $code
     * @return mixed
     */
    public function authenticate($code)
    {
        $request = $this->guzzleAuthClient->request('POST', 'login/oauth/access_token', array(
            'json' => array(
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'code' => $code
            )
        ));

        return json_decode($request->getBody()->getContents());
    }

    /**
     * @param string $username
     * @return mixed
     */
    public function getBasicUserData($username)
    {
        $request = $this->guzzleClient->request('GET', sprintf('users/%s', $username), [
            'auth' => [$this->username, $this->password]
        ]);

        return json_decode($request->getBody()->getContents());
    }

    /**
     * @param string $username
     * @return mixed
     */
    public function getRepositories($username)
    {
        $request = $this->guzzleClient->request('GET', sprintf('/users/%s/repos', $username), [
            'auth' => [$this->username, $this->password]
        ]);

        return json_decode($request->getBody()->getContents());
    }

    /**
     * @param array $languageUrls
     * @return mixed
     */
    public function getUserLanguageStatistic($languageUrls)
    {
        $languages = array();

        foreach ($languageUrls as $languageUrl) {
            $request = $this->guzzleClient->request('GET', $languageUrl, [
                'auth' => [$this->username, $this->password]
            ]);

            $repositoryLanguages = json_decode($request->getBody()->getContents());

            foreach ($repositoryLanguages as $key => $value) {
                if (array_key_exists($key, $languages)) {
                    $languages[$key] += $value;
                } else {
                    $languages[$key] = $value;
                }
            }
        }

        return $languages;
    }
}