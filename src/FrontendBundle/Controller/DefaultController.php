<?php

namespace FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package FrontendBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function authenticateAction(Request $request)
    {
        $code = $request->query->get('code');

        $authToken = $this->get('gr_github_provider')->authenticate($code);

        $this->get('session')->save('github_auth_token', $authToken);

        return $this->redirectToRoute('frontend_homepage');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('github_username', TextType::class)
            ->add('github_password', PasswordType::class)
            ->add('username', TextType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $username = $form->get('username')->getData();
            $githubUsername = $form->get('github_username')->getData();
            $githubPassword = $form->get('github_password')->getData();

            $this->get('session')->set('username', $username);
            $this->get('session')->set('github_username', $githubUsername);
            $this->get('session')->set('github_password', $githubPassword);

            return $this->redirectToRoute('frontend_resume', array('username' => $username));
        }

        return $this->render('FrontendBundle:Default:index.html.twig', array(
                'form' => $form->createView()
            )
        );
    }

    /**
     * @param $username
     * @return Response
     */
    public function resumeAction($username)
    {
        if ($this->get('session')->get('username') !== $username) {
            return $this->redirectToRoute('frontend_homepage');
        }

        $userdata = $this->get('gr_github_provider')->getBasicUserData($username);
        $repositories = $this->get('gr_github_provider')->getRepositories($username);
        $languages = $this->generateLanguagesRatio($repositories);

        return $this->render('FrontendBundle:Default:resume.html.twig', array(
                'username' => $username,
                'repositories' => $repositories,
                'languages' => $languages,
                'userdata' => $userdata
            )
        );
    }

    /**
     * Todo move to service/handler
     * @param array $repositories
     * @return array
     */
    public function generateLanguagesRatio($repositories)
    {
        $languagesUrls = array();
        $languagesSum = 0;

        // get only language urls of repos
        foreach ($repositories as $repository) {
            $languagesUrls[] = $repository->languages_url;
        }

        $languages = $this->get('gr_github_provider')->getUserLanguageStatistic($languagesUrls);

        // calc sum of all language lines
        foreach ($languages as $language) {
            $languagesSum += $language;
        }

        // calc percentage value
        foreach ($languages as $key => $value) {
            $languages[$key] = ($value * 100) / $languagesSum;
        }

        return $languages;
    }
}
